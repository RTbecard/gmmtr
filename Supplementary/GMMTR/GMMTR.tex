\documentclass[11pt]{article}
%%%%%%%%%%%%%%%%% Skeleton %%%%%%%%%%%%%%%%%%
% Font settings
\usepackage[T1]{fontenc}
\usepackage{libertine}
\renewcommand*\ttdefault{lmvtt}
\usepackage{inconsolata}
% Margins
% Set to MSword style margins
\usepackage[margin=1in]{geometry}

% Section Formatting
\usepackage{titlesec}
\titleformat{\section}{\scshape\Large}{\thesection}{1em}{}[\titlerule]{}

% Caption formatting
\usepackage{caption}
\captionsetup[table]{skip=10pt}

% Equations
\usepackage{amsmath}
\mathchardef\mhyphen="2D % Define a "math hyphen"

% Images
\usepackage[table,dvipsnames]{xcolor}
\usepackage{graphicx}

% Floats
\usepackage{float}
% begin{figure}[H] to place figure in text at position

% Figure labelling (inside figures)
\usepackage{stackengine}
\def\stackalignment{l}
% \topinset{label}{image}{from top}{from left}

% Textwrapped figures
\usepackage{wrapfig}
% \begin{wrapfigure}[numberLines]{placement}[overhang]{width}

% Tables
\usepackage{booktabs}
\usepackage{tabularx}
% Fix rowcolors for tabularx
\newcounter{tblerows}
\expandafter\let\csname c@tblerows\endcsname\rownum

% List settings
\usepackage{enumitem}
\setlist{itemsep=0em,parsep=0em}
%Change to alphabetical listing
%\begin{enumerate}[label=(\Alph*)]

% SI unit characters (includiong non-italic)
\usepackage{siunitx}
\usepackage{eurosym}
% Define Euro symbol
\DeclareSIUnit{\pers}{pers}
\DeclareSIUnit{\EUR}{\text{\euro}}
\sisetup{
  per-mode = symbol,
  inter-unit-product = \ensuremath{{}\cdot{}},
  range-phrase=--,
  range-units=single
}
\DeclareSIUnit \amphour {Ah} %Define Amphour

% Code blocks
\usepackage{listings}
\definecolor{gitGrey}{rgb}{0.97,0.97,0.97}
\definecolor{gitRed}{rgb}{0.86,0.09,0.28}
\definecolor{gitGreen}{rgb}{0.07,0.60,.18}
\definecolor{ruleGrey}{rgb}{0.8,0.8,0.8}
\lstdefinestyle{github}{
  belowcaptionskip=1\baselineskip,
  breaklines=true,
  backgroundcolor = \color{gitGrey},
  xleftmargin=0cm,
  extendedchars = true,
  showstringspaces=false,
  basicstyle=\ttfamily\small,
  keywordstyle=\ttfamily\small,
  commentstyle=\itshape\color{gitGreen},
  stringstyle=\color{gitRed},
  numbers=left,
  columns=fullflexible,
  tabsize=2}
  
% Hyper links
\usepackage[color links = true,
  all colors = blue]{hyperref}

% Hanging indents
\usepackage{hanging}
%%%%%%%%%%%%%% Drawing %%%%%%%%%%%%%%%%%%%%
\usepackage{tikz}
\usetikzlibrary{arrows}
\usetikzlibrary{shapes}
\usepackage{pgfplots}
%%%%%%%%%%%%%% Draft settings %%%%%%%%%%%%%
% Line numbering
\usepackage[switch, modulo]{lineno}
%\linenumbers

% Enable highlighting
\usepackage{soul}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\makeatletter
\DeclareCaptionFormat{form}{File\dotfill #3}
\captionsetup[lstlisting]{format=form,singlelinecheck=false,margin=0pt,
		font={tt},labelformat=empty,skip=0pt}
\newcommand{\rcodefile}[1]{
	\filename@parse{#1}
	\lstinputlisting[language=R, style=github,
	caption=\filename@base .\filename@ext]{#1}
}
\newcommand{\cppcodefile}[1]{
	\filename@parse{#1}
	\lstinputlisting[language=C++, style=github,
	caption=\filename@base]{#1}
}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\title{Generalized Mixed Model\\for Responses to Transient Stressors}
\author{James Campbell}

\begin{document}
\maketitle

\section{Overview}

The general idea for the following model was proposed by Len Thomas during a consultation visit in St. Andrews, Scotland on 2019-06-05.
The problem involves examining fish responses to playbacks of seismic survey sounds in a net pen experiment.
In this setup, we have:

\begin{itemize}
	\item Multiple individuals (16), each exposed to a single experimental stimulus.
	\item A trial consists of approximately 22 hours baseline data followed by the 1 hour exposure to stimulus.
	\item Each individual is tracked during the entire trial with 3D positions on a \SI{1}{\hertz} time scale and an accelerometer data logger of \SI{100}{\hertz}.
	\item Three different stimulus levels were used, each level had multiple trials (this was unintentional, and is thus unbalanced).
\end{itemize}

In this setup, we're interested in understanding the magnitude of the intial response to the stimulus in addition to how quickly the individuals return to baseline swimming behaviour.

The problem involves an inherently multivariate response as we can utilize swimming depth, swimming speed, and multiple aggregate accelerometer measures which Erik Tetteroo has derived for us during his bachelor's research project.
To simplify this, we'll first condense the multivariate response into a Mahalanobis distance measure which will transform the problem into a univariate response while preserving the covariance structure of our multinomial response.
There are many ways to calculate Mahalanobis distance; here, we'll formulate the distance using the entire trial for each individual (baseline and exposure periods).
While it may seem intuitive to only use the baseline periods to formulate the Mahalanobis distance, because we're working on relatively short time scales there is a risk that the definition of the baseline swimming behaviour before exposure may be different than that after exposure (baseline drift).
We have thus taken an approach that reduces the chance of type I errors in this context.

Before calculating the Mahalanobis distance, the dataset needs to be downsampled.
We should downsample our movement data so we are able to clearly differentiate the step lengths between resting and swimming behaviours.
Additionally, our accelerometer records raw data at \SI{100}{\hertz} which needs to be converted into aggregate measures per bin.
Thus, we need to select a suitable minimum window size which will allow us to extract useful aggregate measures.
Erik is currently using 5 minute accelerometer bins for his swimming behaviour classifier but suggested that 1 minute bins may be sufficient for this purpose. 
After selecting a suitable accelerometer bin size, our downsampled movement data should be aggregated across bins so our covariate data is on the same temporal scale.

Len proposed a non-linear model that tests an immediate response in Mahalanobis distance followed by an exponential decay in response over time.
In addition to this, we can also add a linear decay over time and null model for comparison.

We are expecting there may be some temporal autocorrelation in our data (even when binned at 1 minute intervals).
We can deal with this be examining autocorrelograms of the Mahalanobis distance over the baseline periods for each individual.
We're expecting to see autocorrelograms with high Pearson's r at low lags, which sharply drop as the lag increases.
Our data should be downsampled so that this initial \textit{low-lag peak} disappears.

For covariates, we can examine the time of day (sun elevation) and the tidal cycle.
For the tidal cycle covariate, it should be converted into periodic values $(0, 2\pi]$ where the value of $2\pi$ represents the time of expected peak activity in behaviour (such as high tide, or dawn).
A visual examination of the Mahalanobis distance is suitable for determining where the peak effects lie in these covariate ranges.
We can then cosine transform these values which will result in a measure ranging from $(0, 1]$ where 1 represents the maximum expected covariate effect.

As we are expecting broad individual variation over our large sample size, we'll use Gaussian random effects here.
Since we're using a Mahalanobis distance formulated on a per individual basis, the random effect of the individual requires some care to interpret in our model.
In this case, we can view the random effect as controlling for a drift in baseline behaviour over time.
If an individual's baseline behaviour slowly changes over the trial period (for a specific example, imagine if the relationship between swimming depth and swimming speed change due to tidal period during a trial for an individual), this \textit{may} result in a less sensitive Mahalanobis distance while showing typical baseline behaviour across the entire trial.
The rational is that due to a drift in baseline behaviour, the definition of the Mahalanobis distance is broadened to describe the entire range of behavioural drift.
To simplify the application of this model, we'll code our likelihood function in \href{https://cran.r-project.org/web/packages/TMB/index.html}{TMB}.
TMB provides automatic differentiation, which essentially automates the process of integrating over the random effects provided they are Gaussian.

\section{Formulation}

Here I'll describe the structure of the models.
This may help in understanding the c++ template for the likelihood function.

Since the Mahalanobis distance is zero bounded, $(0,\infty)$, we're assuming a Gamma error distribution.

$$ \epsilon = \text{Gamma}(k, \theta) $$

As a result, the following models should be log-linked to fit our error distribution:

$$ ln(\mu) = X\beta $$
\textellipsis and the corresponding mean function:
$$ \mu = e^{X\beta} $$

\subsection{Exponential Decay Response}

\begin{equation}\label{eq:expDecay}
	y_{t} = e^{X_a\beta_a + JI + Zu} + \epsilon
\end{equation}

\begin{equation*}
	JI = 
	\begin{cases}
		0, & \text{if}\ t < \tau \\
		X_b\beta_b e^{X_c\beta_c(\tau - t)}, & \text{if}\ \tau \leq t \\
	\end{cases}
\end{equation*}

$t$ is the current time of the trial and $\tau$ is the time of the survey start.
$\beta_b$ and $\beta_c$ are the initial magnitude and strength of decay over time of the acoustic response, respectively, with $X_b$ and $X_c$ as their corresponding design matrices.
$\beta_a$ and $X_a$ are the predictors for the baseline swimming behaviour (such as sun and tidal schedules).
$Z$ is the design matrix for the Gaussian distributed random effect parameter $u$. 
In this formulation, the $\beta_{a}$, $\beta_{b}$, and $\beta_{c}$ each represent vectors of parameters (they may each have intercept and covariate terms).
Thus, this model can be viewed as three linear models combined into a single non-linear model.

Below we'll plot the predicted response with the following parameters: $\beta_a = 0$, $\beta_b = 5$, $\beta_c = 2$, and $\tau = 50$.
Since we know when the start of the exposure occurs, the parameter $\tau$ doesn't need need to be estimated by the model.

\begin{tikzpicture}
	\begin{axis}[
		axis lines = left,
		ymin = -5, ymax = 10,
		width = 0.5\linewidth, height = 0.4\linewidth,
		xlabel = $t$, ylabel = $\ln(\mu)$]
	\addplot [domain=48:50, samples=100, color = red]{0};
	\addplot [domain=50:55, samples=100, color = red]{5*exp(1*(50-x))};
	\draw [<-, color=blue] (axis cs: 50,-1) -- (axis cs: 50,-2) node [below] {$\tau$};
	\draw [|-|, color=blue] (axis cs: 49.8, 5) -- (axis cs: 49.8, 0.1) node [left, midway] {$\beta_b$};
	\draw [|-|, color=blue] (axis cs: 50, 5.5) -- (axis cs: 52, 5.5) node [above, midway] {$\beta_c$};
	\end{axis}
\end{tikzpicture}

\subsection{Linear Decay Response}

Modelling the linear decay will require the same number of parameters as the exponential decay.
In this case, higher values of $\beta_c$ will result in slower decays.

\begin{samepage}

\begin{equation}\label{eq:linearDecay}
	y_{t} = e^{X_a\beta_a + JI + Zu} + \epsilon
\end{equation}

\begin{equation*}
	JI = 
	\begin{cases}
		0, & \text{if}\ t < \tau \\
		\beta_b - {X_c\frac{\beta_b}{\beta_c}(\tau - t)}, & \text{if}\ \tau \leq t < (\tau + \beta_c) \\
		0, & \text{if}\ (\tau - t) \leq t \\
	\end{cases}
\end{equation*}

\end{samepage}

\begin{tikzpicture}
	\begin{axis}[
		axis lines = left,
		ymin = -5, ymax = 10,
		width = 0.5\linewidth, height = 0.4\linewidth,
		xlabel = $t$, ylabel = $\ln(\mu)$]
	\addplot [domain=48:50, samples=100, color = red]{0};
	\addplot [domain=50:52, samples=100, color = red]{5 + ((5/2)*(50-x))};
	\addplot [domain=52:55, samples=100, color = red]{0};
	\draw [<-, color=blue] (axis cs: 50,-1) -- (axis cs: 50,-2) node [below] {$\tau$};
	\draw [|-|, color=blue] (axis cs: 49.8, 5) -- (axis cs: 49.8, 0.1) node [left, midway] {$\beta_b$};
	\draw [|-|, color=blue] (axis cs: 50, 5.5) -- (axis cs: 52, 5.5) node [above, midway] {$\beta_c$};
	\end{axis}
\end{tikzpicture}

\section{TMB Likelihood Template}

The following code was used for the C++ template in our model.
I've generalized the model to work with multiple link functions and distributions.

\cppcodefile{./GTRMM.cpp}

\end{document}
