#include <TMB.hpp>

template<class Type>

// Mean function
Type mean_function(Type x, int link){
  switch(link){
  case 0: return(x);                  // Identity
    break;
  case 1: return(exp(x));             // Log
    break;
  case 2: return(-pow(x, Type(-1)));  // Negative inverse
    break;
  case 3: return(Type(1)/(Type(1) + exp(-x)));    // Logit
    break;
  }
}

// Link function
template<class Type>
Type link_function(Type x, int link){
  switch(link){
  case 0: return(x);                  // Identity
    break;
  case 1: return(log(x));             // Log
    break;
  case 2: return(-pow(x, Type(-1)));  // Negative inverse
    break;
  case 3: return(log(x/(Type(1) - x)));    // Logit
    break;
  }
}

// Error distribution
template<class Type>
Type derror(Type x, Type trials,
            Type u, int distribution,
            Type params){

  switch(distribution){
  case 0: return(dnorm(x, u, params, true));                 // Normal
    break;
  case 1: return(dpois(x, u, true));                         // Poisson
    break;
  case 2: return(dnbinom(x, params, u, true));               // Negative Binomial
    break;
  case 3: return(dgamma(x, params, u/params, true));         // Gamma
    break;
  case 4: return(dbinom(x, trials, u, true));                // Binomial
    break;
  }
}

template<class Type>
Type objective_function<Type>::operator() ()
{

  /********************* Sample VectoRs ***************************/
  // These vectors have a length equal to the number of data points
  DATA_MATRIX(time);              // Time since last exposure
  DATA_VECTOR(y);                 // Measured Mahanalobis distances
  DATA_VECTOR(trials);            // Trials per sample (only for binomial data)
  // Initalize vector for estimated Mahanalobis distance
  vector <Type> y_hat(y.size());
  vector <Type> Nu(y.size());  // Estimated fixed effects


  /******************** DistRibution paRameteRs ********************/
  DATA_INTEGER(link);
  /* 0 = Identity
   * 1 = Log
   * 2 = Negative Inverse
   * 3 = Logit
   */
  DATA_INTEGER(distribution);
  /* 0 = Normal
   * 1 = Poisson
   * 2 = Negative Binomial
   * 3 = Gamma
   * 4 = Binomial
   */
  //*********** Covariates and design matricies *****************/
  // Covariates for baseline swimming behaviour
  PARAMETER_VECTOR(beta_a);  // Baseline swimming coefficients
  DATA_MATRIX(X_a);          // Covariates for baseline swimming

  // Estimated Response to acoustic exposure
  PARAMETER_VECTOR(beta_b);  // Reponse magnitude coefficients
  DATA_MATRIX(X_b);          // Response magnitude covaraites

  PARAMETER_VECTOR(ln_beta_c);  // Response decay coefficients
  DATA_MATRIX(X_c);             // Response decay covariates
  vector <Type> beta_c = exp(ln_beta_c);

  // Random effects
  DATA_IVECTOR(randEff);           // 1 = on, 0 = off
  PARAMETER_VECTOR(uA);            // RandEff (baseline)
  PARAMETER_VECTOR(uB);            // RandEff (magnitude)
  PARAMETER_VECTOR(uC);            // RandEff (decay)

  DATA_MATRIX(Z);                 // Random Effect IDs.  This is a diagonal matrix of 1's
  DATA_IVECTOR(Z_sigma_idx);      // Index of Rand Eff variances (grouping variable)
  // Random effects variances
  PARAMETER_VECTOR(ln_sigma_reA);
  PARAMETER_VECTOR(ln_sigma_reB);
  PARAMETER_VECTOR(ln_sigma_reC);
  vector <Type> sigma_reA = exp(ln_sigma_reA);
  vector <Type> sigma_reB = exp(ln_sigma_reB);
  vector <Type> sigma_reC = exp(ln_sigma_reC);

  /*********** Error distribution parameters ********************/
  /*  Gamma distibution, parameters are log transformed to restrict them to
       positive values.  Shape is fixed and scale is calculated from mean */
  PARAMETER(ln_errorParam);

  /*********** AR(1) parameters ********************/
  /*  These are only used in the function gmmtrAR */
  DATA_INTEGER(AR_order);         // switch 0 indicates AR processes are ignored
  PARAMETER_VECTOR(logit_phi);    // vector of AR coefficients
  vector <Type> phi = (1 / (1 + exp(-logit_phi)));

  PARAMETER(ln_sigma_reAR);   // SD for non-gaussian AR process
  Type sigma_reAR = exp(ln_sigma_reAR);
  // Random varaible holding non-gaussian AR1 latent variables
  PARAMETER_VECTOR(uAR);

  // Helper vars
  matrix <Type> a, b, c;
  vector <Type> JI, eps;

  // std::cout << "phi: " << phi << "\n";

  /****************************************************************************/
  /********************   START ROUTINE   *************************************/
  /****************************************************************************/
  // Initialize the negative log likelihood
  Type nll = 0.0;
  //parallel_accumulator<Type> nll(this); // parallel processing
  // parallel causes memory leak and will overload ram when bootstrapping

  /**************** Calculate Random Effects likelihood ***********************
   * if sd values are set to 0, these random effets will be skipped in the
   * likelihood function
   */
  int j;
  if(Z_sigma_idx.size() > 0){ // Skip if random effects disabled
    for(int i = 0; i < uA.size(); i++){ // Loop through random effect intercepts
      j = Z_sigma_idx(i);  // get index for random effect sigma

      // baseline
      if(randEff(0) == 1){
        nll -= dnorm(uA(i), Type(0), sigma_reA(j), true);
      }
      // magnitude
      if(randEff(1) == 1){
        nll -= dnorm(uB(i), Type(0), sigma_reB(j), true);
      }
      // decay
      if(randEff(2) == 1){
        nll -= dnorm(uC(i), Type(0), sigma_reC(j), true);
      }
    }
  }

  // std::cout << "uA: "  << uA << "\n";
  // std::cout << "uB: "  << uB << "\n";
  // std::cout << "uC: "  << uC << "\n";

  // std::cout << "RandEff nll: "  << nll << "\n";

  /***************** Calculate fixed effects **********************************/
  if(Z_sigma_idx.size() > 0){
    // Baseline Swimming Covaraites
    a = X_a*beta_a + Z*uA;
    // Calculate JI: Response to exposure
    b = X_b*beta_b + Z*uB; // Response magnitude
    c = X_c*ln_beta_c + Z*uC; // Response decay
  }else{
    a = X_a*beta_a;
    // Calculate JI: Response to exposure
    b = X_b*beta_b; // Response magnitude
    c = X_c*ln_beta_c; // Response decay
  }

  vector <Type> exp_c = exp(c.vec());

  // std::cout << "a: " <<  a(0,0) << "\n";
  // std::cout << "b: " <<  b(0,0) << "\n";
  // std::cout << "c: " <<  c(0,0) << "\n";

  vector <Type> time_vec = time.col(0);
  JI = b.vec()*exp(exp_c*(-time_vec));

  // std::cout << "JI: " <<  JI(0,0) << "\n";

  // Set response effect before exposures to 0
  for(int i = 0; i < JI.size(); i += 1){
    // Could not get this working without loop
    if(time(i,0) < 0){
      JI[i] = 0;
    }
  }

  /********** Get Model Residuals ********/
  // Calc covariate effects for each sample
  Nu = a.vec() + JI;


  // std::cout << "Nu: " <<  Nu(0) << "\n";

  // Nu is the mean estimate (ignoring AR processes)
  // Z and u are 0 if random effects are disabled
  /************ likelihood ***************/
  // Residuals
  Type errorParam = exp(ln_errorParam);

  /*********************** calculate Residual likelihood **********************/
  vector <Type> ar_coef(y.size());  // temp var for holding AR effect on response estimates
  vector <Type> predicted(y.size());

  if(AR_order == 0){  //------------------------------------------Non-AR process
    for(int i = 0; i < y.size(); i++){
      y_hat(i) = mean_function(Nu(i), link);
      predicted(i) = mean_function(Nu(i), link);
      nll -= derror(y(i), trials(i), y_hat(i), distribution, errorParam);
      // std::cout << "i: "  << i <<
      //   ", y_hat: " << y_hat(i) <<
      //   ", Nu(i): " << Nu(i) <<
      //   ", nll: " << nll << "\n";
    }
  }else if(distribution == 0 & link == 0){  //----------Gaussian AR(n) residuals
    for(int i = 0; i < y.size(); i++){
      // Skip if there is a break in the track
      if(time(i, 1) == 0){

        // Calculate phi (AR term) effect
        ar_coef[i] = 0;
        for(int j = 1; j <= AR_order; j++){  // loop through AR coeffcients
          // Add AR residual effects
          ar_coef[i] = ar_coef[i] + phi(j-1)*(y(i-j) - Nu(i-j));
        }
        predicted(i)  = Nu(i);
        y_hat(i) = Nu(i) + ar_coef[i];

        // Observation likelihood
        nll -= derror(y(i), trials(i), y_hat(i), distribution, errorParam);
      }
    }
  }else{  //----------------------------------------non-Gaussian AR(n) residuals
    for(int i = 0; i < y.size(); i++){
      // Only process marked segments
      if(time(i, 1) == 1){
        // likelihood is not evaluated for esimates before AR(n) process
        ar_coef(i) = uAR(i);
        y_hat(i) = mean_function(Nu(i), link);
        predicted(i) = mean_function(Nu(i), link);
      }else{
        // Calculate AR effect
        ar_coef(i) = uAR(i);
        for(int j = 1; j <= AR_order; j++){  // loop through AR coeffcients
          // Add AR residual effects
          ar_coef(i) = ar_coef(i) + phi(j-1)*ar_coef(i-j);
        }

        y_hat(i) = mean_function(Nu(i) + ar_coef(i), link);
        predicted(i) = mean_function(Nu(i), link);

        // std::cout << "ar_coef: "  << ar_coef(i) <<
        //   ", y_hat(i): " << y_hat(i) <<
        //     ", Nu(i): " << Nu(i) <<
        //       ", uAR(i): " << uAR(i) << "\n";

        // Observation likelihood
        nll -= derror(y(i), trials(i), y_hat(i), distribution, errorParam);
      }
    }

    // latent effect likelihood (Guassian, mean = 0)
    for(int i = 0; i < y.size(); i++){
      nll -= dnorm(uAR(i), Type(0), sigma_reAR, true);
    }
  }

  // std::cout << "FixedEff nll: "  << nll << "\n";

  /************** Report values **************/
  REPORT(beta_a);
  REPORT(beta_b);
  REPORT(beta_c);
  REPORT(errorParam);
  REPORT(sigma_reA);
  REPORT(sigma_reB);
  REPORT(sigma_reC);
  REPORT(sigma_reAR);
  REPORT(y_hat);
  REPORT(uA);
  REPORT(uB);
  REPORT(uC);
  REPORT(uAR);
  REPORT(phi);
  REPORT(predicted);

  return nll;
}
