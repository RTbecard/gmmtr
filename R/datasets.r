#' Atlantic Cod Positions from an Outdoor Netpen Experiment
#'
#' In this dataset, 6 individuals have been exposed to a simulated seismic survey.
#' Each trial consisted of a single individual in the pen with approximately 20 hours of baseline measurements before the exposure onset which was timed to occure at high tide.
#' The exposure consisted of a 1 hour playback of recorded seismic survey pulses with 6 a second inter pulse interval.
#'
#' The netpen was approximately 7m in diameter and depths ranged from 4-5m, depending on tide.
#'
#' This dataset was downsampled, resulting in positions approximated every 5 seconds.
#' The z dimension was removed from the dataset, as the hydrophone configuration did not allow for accurate depth measurements.
#'
#'@field x x position of the fish in meters
#'@field y y position of the fish in meters
#'@field speed The instantanious 2D speed calcualted form the x and y positions.
#'@field Tide Tide-related changes in water depth.
#'These values were taken from weather services, so they may not reflect absolute changes in depth at the site.
#'@field Period Factor describing, before, after, or during periods of the simulated seismic survey exposure.
#'@field fish_id Unique ID for experimental individuals.
#'@field time Time in seconds during a trial
#'@field time.event Time in seconds since the last exposure onset.  Negative values are allowed.
#'@field time.segment Unique identifier for contigious segments of positioning.
#'Breaks in positioning data are a result from periods of low detectability of the acoustic tag in the hydrophone array.
#'@md
#' @format data.frame
"Netpen.HTI.analysis"
