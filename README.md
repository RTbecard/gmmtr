Generalized Mixed Models for Transient Responses
---

This package contains a mixed effects model specifically created for examining time-series data describing responses of animals to transient events.
Random effects (random intercept) and temporal autocorrelation are supported.

### To-do list

* Rework documentation
* Make nicer vingette